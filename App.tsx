import * as React from 'react';

import { AppNavigator } from './src/navigation';
import { AppStatusbar } from './src/componets';
import { StyleSheet, View, Text, TextInput } from 'react-native'
import { Colors } from './src/theme';

export default function App() {

  React.useEffect(() => {

    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false

    if (TextInput.defaultProps == null) TextInput.defaultProps = {};
    TextInput.defaultProps.allowFontScaling = false


  }, [])


  return (
    <View style={styles.container}>

      <AppStatusbar translucent />
      <AppNavigator />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white, flex: 1
  }
})
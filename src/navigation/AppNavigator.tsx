/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-03 00:57:37
 */

import 'react-native-gesture-handler';

import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionSpecs, TransitionPresets } from '@react-navigation/stack';
import {
    Splash, SlideOne, SlideTwo, Login, Otp, Gender, DOB, UploadImage,
    TermsConditions, Finish, Category, Tabs, Menu, ServiceProvider
} from '../screens'


import Address from '../screens/auth/Address';

const Stack = createStackNavigator();


function App() {

    return (
        <View style={{ flex: 1 }}>

            <NavigationContainer>

                <Stack.Navigator
                    initialRouteName={"Splash"}>

                    <Stack.Screen name="Splash" component={Splash}
                        options={options} />

                    <Stack.Screen name="SlideOne" component={SlideOne}
                        options={options} />

                    <Stack.Screen name="SlideTwo" component={SlideTwo}
                        options={options} />

                    <Stack.Screen name="Login" component={Login}
                        options={options} />

                    <Stack.Screen name="Otp" component={Otp}
                        options={options} />

                    <Stack.Screen name="Gender" component={Gender}
                        options={options} />

                    <Stack.Screen name="DOB" component={DOB}
                        options={options} />

                    <Stack.Screen name="UploadImage" component={UploadImage}
                        options={options} />

                    <Stack.Screen name="TermsConditions" component={TermsConditions}
                        options={options} />

                    <Stack.Screen name="Address" component={Address}
                        options={options} />

                    <Stack.Screen name="Finish" component={Finish}
                        options={options} />

                    <Stack.Screen name="Category" component={Category}
                        options={options} />

                    <Stack.Screen name="Home" component={Tabs}
                        options={options} />

                    <Stack.Screen name="Menu" component={Menu}
                        options={options} />

                    <Stack.Screen name="ServiceProvider" component={ServiceProvider}
                        options={options} />


                </Stack.Navigator>
            </NavigationContainer>

        </View>
    );
}

const options = () => ({
    headerShown: false,
    ...TransitionPresets.SlideFromRightIOS,
    transitionSpec: {
        open: TransitionSpecs.TransitionIOSSpec,
        close: TransitionSpecs.TransitionIOSSpec,
    }

})



export default App;
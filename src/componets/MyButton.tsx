import React from 'react';
import { Button } from 'react-native-paper';
import { StyleProp, ViewStyle, TextStyle, StyleSheet, Dimensions } from 'react-native';
import TextView from './TextView';
import { Colors } from '../theme';



type buttonProps = {
    label: string,
    onPress(): void,
    style?: StyleProp<ViewStyle>,
    textStyle?: StyleProp<TextStyle>,
    lableStyle?: StyleProp<TextStyle>,
    loading?: boolean
}

var { height, width } = Dimensions.get('window')


const MyButton = ({ label, loading = false, onPress, style, textStyle, lableStyle }: buttonProps) => (

    <Button
        mode="contained" onPress={onPress}
        dark={true}
        labelStyle={[{ color: "#fff",  height: width / 17, textAlignVertical:"center"}, lableStyle]}
        loading={loading}
        style={[styles.button, style]}>

        <TextView
            text={label}
            style={[{ textTransform: 'capitalize', }, textStyle]}
        />
    </Button>
)


export default React.memo(MyButton)

var styles = StyleSheet.create({

    button: {
        backgroundColor: Colors.pink,
        height: width / 10, justifyContent: 'center', alignSelf: 'center', 
        width: width - 200, borderRadius: width / 20
    }
})
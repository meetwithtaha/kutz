/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-17 01:01:21
 */


import React, { useEffect } from 'react';
import { Text, Dimensions, StyleSheet, TouchableOpacity, View, StyleProp, ViewStyle } from 'react-native'
import { Colors } from '../theme'

var { height, width } = Dimensions.get('window')

// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { IconButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

type BackArrowType = {
    style?: StyleProp<ViewStyle>,
    tintColor?: string
}

type IconBottonType = {
    size: number,
    color: string
}

const BackArrow = ({ style, tintColor }: BackArrowType) => {

    const navigation = useNavigation()

    const onPress = () => {
        navigation.goBack()
    }

    return (

        <IconButton
            style={style}
            icon={({ size, color }: IconBottonType) => {
                return <Text>Back</Text>
                // <Icon name="arrow-left" size={35} color={tintColor ? tintColor : Colors.black} />
            }}
            size={25}
            onPress={onPress}
        />


    )
}


export default BackArrow;

const styles = StyleSheet.create({

    btnContainer: {
        backgroundColor: Colors.blue, marginHorizontal: 50,
        height: 50, justifyContent: "center", alignItems: "center",
        borderRadius: 7,

    },
    BackArrowText: {
        color: Colors.white, fontSize: width / 25
    }

})
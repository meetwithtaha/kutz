/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-05 00:55:59
 */


import React, { useRef, memo, useMemo } from "react";
import { Animated, View, StyleSheet, PanResponder, Text, Image, Dimensions } from "react-native";
import { Icons } from "../theme";

var { width, height } = Dimensions.get('window')

type CursorProps = {
    onNext(): void,
    onPrevious(): void
    position: string
}

const distanceCenter = 103
const distanceStart = 208

var App = ({ position, onNext, onPrevious }: CursorProps) => {
    // const maxDistance = 103
    const direction = position == "center" ? true : false
    const MaxDistance = direction ? distanceCenter : distanceStart
    const MinDistance = direction ? -distanceCenter : 0


    const pan = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;

    // const panResponder = useRef(
    const panResponder = useMemo(() => PanResponder.create({
            onMoveShouldSetPanResponder: () => true,

            onPanResponderGrant: () => {
                pan.setOffset({
                    x: pan.x._value,
                    y: pan.y._value
                });
            },
            onMoveShouldSetPanResponderCapture: (e, { dx }) => {
                // This will make it so the gesture is ignored if it's only short (like a tap).
                // You could also use moveX to restrict the gesture to the sides of the screen.
                // Something like: moveX <= 50 || moveX >= screenWidth - 50
                // (See https://facebook.github.io/react-native/docs/panresponder)
                return Math.abs(dx) > MaxDistance;
            },
            onPanResponderMove: (evt, gestureState) => {


                const dxCapped = Math.min(Math.max(parseInt(gestureState.dx + ''), MinDistance), MaxDistance);

                const values = {}
                values.dx = dxCapped
                values.dy = gestureState.dy

                return Animated.event([null, {
                    dx: pan.x,
                    dy: pan.y,
                }], { useNativeDriver: false })(evt, values);

            },
            onPanResponderRelease: (evt, gestureState) => {

                pan.flattenOffset();

                if (gestureState.dx < MaxDistance) {

                    if (onPrevious && !(gestureState.dx >= MinDistance)) {
                        onPrevious()
                    }

                } else {
                    onNext()
                }

                Animated.spring(pan, {
                    toValue: 0,
                    tension: 1,
                    useNativeDriver: false
                }).start();

            }
        })
    , [])
    // ).current;

    return (
        <View style={styles.container}>


            <Animated.Image source={Icons.bar_handle}
                style={{
                    height: width / 6.5, width: width / 6.5,
                    resizeMode: "contain", alignSelf: direction ? "center" : "flex-start",
                    transform: [{ translateX: pan.x }]
                }}
                {...panResponder.panHandlers}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    titleText: {
        fontSize: 14,
        lineHeight: 24,
        fontWeight: "bold"
    },
    box: {
        height: 150,
        width: 150,
        backgroundColor: "blue",
        borderRadius: 5
    }
});

export default App;
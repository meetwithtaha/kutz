import React from 'react';
import { StyleSheet, View,  Image, Dimensions, ImageSourcePropType } from 'react-native';
import { Colors, GlobalStyles, Icons } from '../theme';

type propTypes = {
  children: JSX.Element
  bgImage: ImageSourcePropType
}

const { height, width } = Dimensions.get('window')


const CurveView = ({ children, bgImage }: propTypes) => {



  return (
    <View style={styles.container}>

      <Image
        source={bgImage}
        style={styles.bgImage}
      />

      <View style={GlobalStyles.curveView}>

        {children}

      </View>

    </View>
  )
}

export default CurveView

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  bgImage: { height: height / 2, width: width, resizeMode: "cover", marginTop: -10 },


})
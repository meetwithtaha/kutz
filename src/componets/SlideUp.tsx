/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-03 01:10:09
 */


import React, { useState, useRef, useEffect, memo } from 'react';
import { StyleSheet, Text, View, Animated } from 'react-native';


const FadeUpView = ({ children, style } : any) => {

    let opacity = useRef(new Animated.Value(0))
    let translateY = useRef(new Animated.Value(150))
    useEffect(() => {
        Animated.parallel([
            Animated.timing(opacity.current, {
                toValue: 1,
                duration: 500,
                useNativeDriver: true
            }),
            Animated.timing(translateY.current, {
                toValue: 0,
                duration: 500,
                useNativeDriver: true
            }),
        ]).start()
    })


    return (

        <Animated.View style={[
            {
                opacity: opacity.current,
                transform: [{ translateY: translateY.current }]
            }, style]}>
            {children}
        </Animated.View>
    )

}

export default FadeUpView




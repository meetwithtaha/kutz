/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-03-30 19:42:28
 */


import React, { useEffect } from 'react';
import {  Text, Dimensions, StyleSheet, TouchableOpacity } from 'react-native'
import { Colors } from '../theme'

var { height, width } = Dimensions.get('window')

const Button = ({ style, onPress, title }) => {

    return (

        <TouchableOpacity activeOpacity={.8} style={[styles.btnContainer, style]} onPress={onPress}>

            <Text style={styles.buttonText}>{title}</Text>

        </TouchableOpacity>

    )
}


export default Button;

const styles = StyleSheet.create({

    btnContainer: {
        backgroundColor: Colors.blue, marginHorizontal: 50,
        height: 50, justifyContent: "center", alignItems: "center",
        borderRadius: 7, 

    },
    buttonText: {
        color: Colors.white, fontSize: width / 25
    }

})
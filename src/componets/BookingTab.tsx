import * as React from "react"
import { View, Dimensions, InteractionManager, StyleSheet, TouchableOpacity } from "react-native"
import Animated from "react-native-reanimated"
import TextView from "./TextView"
import { Colors } from "../theme"

const { height, width } = Dimensions.get('window')

const TabBar = (props: any) => {

    const { navigationState, descriptors, navigation, position } = props

    return (

        <View style={styles.mainContainer}>

            {navigationState.routes.map((route, index) => {

                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const inputRange = navigationState.routes.map((_, i) => i);
                const opacity = Animated.interpolate(position, {
                    inputRange,
                    outputRange: inputRange.map(i => (i === index ? 1 : .5)),
                });

                const isFocused = navigationState.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                return (
                    <Animated.View style={[styles.container, { backgroundColor: isFocused ? Colors.pink : Colors.orange }]}>

                        <TouchableOpacity onPress={onPress}>

                            <TextView type={"normalRg"} text={label} style={{ color: Colors.white }} />

                        </TouchableOpacity>

                    </Animated.View>

                )
            })}


        </View>

    )
}

export default TabBar

const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: "row",
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor : Colors.orange
    },
    container: {
        height: width / 12,
        justifyContent: 'center', backgroundColor: "#ccc", borderRightColor: Colors.white,
        paddingLeft: width / 90, paddingRight: width / 90
        
    }
})
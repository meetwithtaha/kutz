import React, { } from 'react';
import { View, StyleSheet, Dimensions, StyleProp, ViewStyle, Image } from 'react-native';
import { Colors, Icons } from '../theme';
import TextView from './TextView';
import { Avatar } from 'react-native-paper';

const { height, width } = Dimensions.get('window')

type ServiceCartType = {
    style?: StyleProp<ViewStyle>
}

const ServiceCart = ({ style }: ServiceCartType) => {

    return (
        <View style={styles.container}>

            <View style={styles.row}>

                <Image
                    source={Icons.groom}
                    style={styles.image}
                />

                <View style={{ flex: 1, marginLeft: width / 60 }}>

                    <TextView text="Father & Child Haircut Combo" style={{ fontSize: width / 30, marginTop: width / 60}} />

                    <View style={styles.row}>

                        <View style={{ flex: 1 }}>

                            <View style={styles.row}>

                                <Image
                                    source={Icons.clock}
                                    style={styles.clock}
                                />

                                <TextView type="small" text="60 min" style={{ marginLeft: width / 80, color: Colors.orange }} />

                            </View>

                            <TextView text="PKR 500/-" style={{ color: Colors.green }} />

                        </View>

                        <Image
                            source={Icons.rating_circle}
                            style={styles.rating}
                        />

                    </View>

                    <View style={[styles.row, { alignSelf: "flex-end", marginRight: width / 25, marginTop: width / 50 }]}>

                        <View style={styles.row}>

                            <Image
                                source={Icons.ic_info}
                                style={styles.clock}
                            />

                            <TextView text="View Details" style={{ marginLeft: width / 80 }} />

                        </View>

                        <View style={[styles.row, { marginLeft: width / 80 }]}>

                            <Image
                                source={Icons.ic_note}
                                style={styles.clock}
                            />

                            <TextView text="Customize" style={{ marginLeft: width / 80 }} />

                        </View>



                    </View>

                </View>

            </View>

            <View style={styles.bookContainer}>
                <Image
                    source={Icons.book_}
                    style={styles.BOOK}
                />

                <TextView text="Book Now" style={styles.bookNow} />

            </View>

        </View >
    )
}

export default ServiceCart

const styles = StyleSheet.create({

    container: {
        width: width - 30, alignSelf: "center",
        marginTop: width / 20, borderWidth: 1.5, borderColor: Colors.pink, borderRadius: width / 30
    },

    image: {
        width: width / 5, height: width / 3.9, resizeMode: "cover", marginLeft: width / 30,
    },

    rating: {
        width: width / 6.5, height: width / 6.5, resizeMode: "contain", marginRight: width / 25,
    },

    clock: {
        width: width / 20, height: width / 20, resizeMode: "contain"
    },

    BOOK: {
        width: width / 25, height: width / 25, resizeMode: "contain"
    },

    row: { flexDirection: "row", alignItems: "center", },

    bookContainer: {
        backgroundColor: Colors.pink,
        height: width / 15, width: width - 32,
        borderRadius: width / 60, flexDirection: "row", alignItems: "center",
        justifyContent: "center",
    },

    bookNow: { marginTop: 2, marginLeft: width / 80, color: Colors.white }

})
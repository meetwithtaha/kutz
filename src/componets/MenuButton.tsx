/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-26 21:41:19
 */

import React, { memo } from 'react';
import { Dimensions, StyleSheet, View, Image, StyleProp, ViewStyle } from 'react-native'
import { Colors, Icons } from '../theme'
import { IconButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

var { width } = Dimensions.get('window')

type IconBottonType = {
    size: number,
    color: string
}

const SearchHeader = () => {

    const navigation = useNavigation()

    const onPress = () => {
        navigation.navigate('Menu')
    }


    return (
        <>

            <IconButton
                icon={({ size }: IconBottonType) => {
                    return <Image
                        source={Icons.menu}
                        style={{ height: size, width: size, resizeMode: "contain", }}
                    />
                }}
                size={width / 13}
                onPress={onPress}
            />
        </>
    )
}


export default memo(SearchHeader);


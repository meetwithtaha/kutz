/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-04 20:05:02
 */


import React, { useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, Text, StyleProp, ViewStyle, Animated, } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../theme';
import { Cursor } from '../componets';
import TextView from './TextView';

var { height, width } = Dimensions.get('window')

type SliderProps = {
    style?: StyleProp<ViewStyle>
    onNext(): void
    onPrevious?(): void
    position?: string
}


var Slider = ({ position = "center", style,
    onNext, onPrevious,  }: SliderProps) => {



    return (
        <View style={style}>


            <Image source={Icons.bar}
                style={GlobalStyles.sliderView} />

            <Cursor position={position} onNext={onNext} onPrevious={onPrevious} />

        </View>
    )

}


export default Slider;


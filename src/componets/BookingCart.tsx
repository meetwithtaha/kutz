import React, { } from 'react';
import { View, StyleSheet, Dimensions, StyleProp, ViewStyle } from 'react-native';
import { Colors, Icons } from '../theme';
import TextView from './TextView';
import { Avatar } from 'react-native-paper';

const { height, width } = Dimensions.get('window')

type BookingCartType = {
    style?: StyleProp<ViewStyle>
}

const BookingCart = ({ style }: BookingCartType) => {

    return (
        <View style={styles.container}>

            <TextView text="11:33" style={{ flex: .11 }} />

            <View style={[styles.cart, style]}>

                <View style={{ flexDirection: "row" }}>

                    <View style={{ flex: 1, marginLeft: width / 60 }}>

                        <TextView text="Booking ID" numberOfLines={1} type="heading_20" style={styles.otherText} />
                        <TextView text="Cash Payment" numberOfLines={1} type="small" style={styles.otherText} />

                    </View>

                    <View style={{ marginRight: width / 60 }}>
                        <TextView text="Booking Status" type="small" style={[styles.otherText, { textAlign: "right" }]} />
                        <TextView text="Completed" type="small" style={[styles.otherText, { textAlign: "right" }]} />
                    </View>

                </View>

                <View style={{ flexDirection: "row", marginLeft: width / 60 }}>

                    <TextView text="Total Amount" type="small" numberOfLines={1} style={[styles.otherText, { flex: 1 }]} />
                    <TextView text="PKR 500/-" numberOfLines={1} type="small" style={[styles.otherText, { marginRight: width / 60 }]} />

                </View>

                <View style={styles.line} />

                <View style={styles.avatar}>

                    <Avatar.Image size={width / 8} source={Icons.male} />
                    <TextView text="Taha Shaikh HCB 391 Shah Faisal Colony Katcha" type="heading_20"
                        numberOfLines={1}
                        style={styles.name}  />

                </View>

            </View>

        </View>
    )
}

export default BookingCart

const styles = StyleSheet.create({

    container: {
        flexDirection: "row", alignItems: "center", 
        marginHorizontal: width / 60, marginTop: width / 90
    },
    cart: {
        flex: .9, height: width / 2.2, marginTop: width / 20,
        backgroundColor: "green", borderRadius: 10, justifyContent: "center"
    },
    line: {
        width: width / 1.3, height: 2,
        backgroundColor: Colors.white, alignSelf: "center", marginTop: width / 60
    },
    avatar: { flexDirection: "row", alignItems: "center", marginLeft: width / 30, marginTop: width / 40 },
    name: { color: "white", textAlign: "left", marginLeft: width / 30, flex: 1 },
    otherText: { color: "white", textAlign: "left" },

})
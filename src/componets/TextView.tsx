import React, { useEffect } from 'react'
import { Text, StyleProp, TextStyle, StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../theme'

const { height, width } = Dimensions.get('window')

type TextViewProps = {
    id?: string | 'custom'
    type?: 'small' | 'heading_20' | 'headingMid' | 'normal' | 'mini_heading22' | 'description13' | 'mini_description' | 'normalRg' | 'normalMd' | 'heading'
    style?: StyleProp<TextStyle>
    onPress?(): void,
    text?: string,
    numberOfLines?: number

}

const TextView = ({ id, type = 'normal', style, onPress, text, numberOfLines, }: TextViewProps) => {

    // let translation = useTranslation()
    let styleByType = getStyleByType(type)

    return <Text
        onPress={onPress}
        numberOfLines={numberOfLines}
        style={[styleByType, style]}>
        {text}
    </Text>
}

let getStyleByType = (type: string): StyleProp<TextStyle> => {
    switch (type) {
        case 'heading':
            return styles.heading

        case 'mini_heading22':
            return styles.mini_heading

        case 'description13':
            return styles.description

        case 'mini_description':
            return styles.mini_description

        case 'normalRg':
            return styles.normalRg

        case 'normalRg':
            return styles.normalRg

        case 'headingMid':
            return styles.headingMid
        case 'heading_20':
            return styles.heading_20
        case 'small':
            return styles.small
        case 'normalMd':
            return styles.normalMd


        default:
            return styles.normal
    }

}
const styles = StyleSheet.create({
    heading: {
        // fontSize: 26, 
        fontSize: width / 13,
        // fontFamily: 'PoppinsRegular', 
        color: Colors.orange, textAlign: 'center'
    },
    headingMid: {
        fontSize: 18, 
        // fontFamily: 'PoppinsLight',
    },
    mini_heading: {
        // fontSize: 22, fontFamily: 'PoppinsLight', textAlign: 'center'
        fontSize: width / 18.5, 
        // fontFamily: 'PoppinsLight', 
        textAlign: 'center'
    },
    heading_20: {
        fontSize: width / 20, 
        // fontFamily: 'PoppinsRegular', 
        textAlign: 'center'
    },
    description: {
        // fontSize: 13, fontFamily: 'PoppinsRegular', textAlign: 'center', color: "#666666"
        fontSize: width / 30, 
        // fontFamily: 'PoppinsRegular', 
        textAlign: 'center', color: "#666666"
    },
    mini_description: {
        // fontSize: 12, fontFamily: 'PoppinsRegular', color: "#9F9F9F"
        fontSize: width / 30, 
        // fontFamily: 'PoppinsRegular', 
        color: "#9F9F9F"
    },
    normalRg: {
        fontSize: width / 28, 
        // fontFamily: "PoppinsRegular"
    },

    normalMd: {
        // fontSize: 14, 
        fontSize: width / 30,
        // fontFamily: "PoppinsMedium"
    },

    normalMini: {
        fontSize: 15, // fontFamily: 'light'
    },

    normal: {
        // fontSize: 16, fontFamily: 'PoppinsRegular',
        fontSize: width / 25, 
        // fontFamily: 'PoppinsRegular',
    },

    small: {
        fontSize: width / 35, 
        // fontFamily: 'PoppinsRegular', 
    },

    extraLight: {
        fontSize: 13, // fontFamily: 'extraLight'
    },



});


export default TextView
import React from 'react';
import { View, Platform, StatusBar, Dimensions } from 'react-native';
import { Colors } from '../theme';
import { getStatusBarHeight } from 'react-native-status-bar-height';

interface propTypes {
  dark?: boolean,
  translucent?: boolean
  darkBrown?: boolean
}
const { height, width, } = Dimensions.get('window')

const AppStatusbar = ({ dark, translucent = false, darkBrown = false }: propTypes) => {

  let isAndroid = Platform.OS == "android" ? true : false;

  // return (
  //   <>

  //     <StatusBar
  //       translucent={translucent}
  //       barStyle={dark ? 'light-content' : 'dark-content'}
  //       backgroundColor={translucent ? 'transparent' : darkBrown ? Colors.brown : 'white'} />

  //   </>

  // )

  const bgColor = translucent ? 'transparent' : darkBrown ? Colors.brown : 'white'

  if (isAndroid) {
    return (
      <StatusBar translucent={translucent}
        barStyle={dark ? 'light-content' : 'dark-content'}
        backgroundColor={bgColor} />
    )
  } else {
    if (translucent) {

      return (
        <StatusBar barStyle={dark ? 'light-content' : 'dark-content'}
          backgroundColor={bgColor} />
      )
    }

    return (
      <View style={{
        height: getStatusBarHeight(), backgroundColor: bgColor
      }}>

        <StatusBar barStyle={dark ? 'light-content' : 'dark-content'}
          backgroundColor={bgColor} />

      </View>
    )
  }
}

export default AppStatusbar
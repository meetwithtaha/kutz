/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-26 21:41:19
 */

import React, { memo } from 'react';
import { Dimensions, StyleSheet, View, Image, StyleProp, ViewStyle } from 'react-native'
import { Colors, Icons } from '../theme'
import { IconButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { TextInput, MenuButton } from '.';

var { width } = Dimensions.get('window')

type IconBottonType = {
    size: number,
    color: string
}

type SearchHeaderType = {
    style?: StyleProp<ViewStyle>
}

const SearchHeader = ({ style }: SearchHeaderType) => {


    return (
        <>
            <View style={[{
                flexDirection: "row", alignItems: "center",
                paddingHorizontal: width / 30
            }, style]}>

                <MenuButton />

                <TextInput placeholder={"Search salon, spa and barber"}
                    onChangeText={() => { }}
                    value={''}
                    search
                    style={{
                        marginTop: 0,
                        flex: 1, marginHorizontal: width / 60, borderRadius: width / 40
                    }}
                />

            </View>

        </>
    )
}


export default memo(SearchHeader);


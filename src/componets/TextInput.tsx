/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-06 21:35:23
 */



import React, { memo, useState } from 'react';
import { View, StyleSheet, Image, Dimensions, Text, TextInput, ViewStyle, StyleProp, KeyboardType, TouchableOpacity } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../theme';
// import Icon from 'react-native-vector-icons/AntDesign';

var { height, width } = Dimensions.get('window')


type TextinputProps = {
    placeholder: string,
    onChangeText(value: string): void,
    value?: string
    style?: StyleProp<ViewStyle>
    icon?: boolean
    onCountrySelect?(): void
    country?: boolean
    keyboardType?: KeyboardType
    onClear?(): void
    numberOfLines?: number
    search?: boolean
}

var CountryType = {
    "cca2": "PK", "currency": ["PKR"], "callingCode": ["92"],
    "region": "Asia", "subregion": "Southern Asia", "flag": "flag-pk",
    "name": "Pakistan"
}


import CountryPicker from 'react-native-country-picker-modal'

const Input = ({ search = false, country = false,
    placeholder, keyboardType, onChangeText, value, style,
    icon, onClear, numberOfLines = 1 }: TextinputProps) => {

    const [countryDetails, setCountryDetails] = useState(CountryType)

    const onSelect = (country: any) => {
        setCountryDetails(country)
    }

    return (
        <View style={[styles.inputContainer, style]}>

            {country &&
                <View style={{
                    flexDirection: "row",
                    alignItems: "center", borderRightWidth: 1.5, paddingRight: 7,
                    borderRightColor: Colors.pink
                }}>

                    <CountryPicker
                        countryCode={countryDetails.cca2}
                        withFilter={true}
                        withCallingCodeButton={true}
                        // visible={countryPicker}
                        onSelect={onSelect}

                    />

                    <Image
                        source={Icons.arrow_down}
                        style={{ width: width / 30, height: width / 30, resizeMode: "contain", marginLeft: 5 }}
                    />

                </View>}

            {/* {search &&
                <Icon name="search1" size={width / 18} color={Colors.black} />} */}

            <TextInput
                placeholder={placeholder}
                value={value}
                numberOfLines={numberOfLines}
                style={styles.inputText}
                placeholderTextColor={"#000"}
                keyboardType={keyboardType}
                onChangeText={onChangeText}
            />


            {icon && value != "" &&
                <TouchableOpacity onPress={onClear}>

                    <Image
                        source={Icons.Close}
                        style={{ width: width / 18, height: width / 18, resizeMode: "contain" }}
                    />
                </TouchableOpacity>

            }

        </View>

    )

}


export default memo(Input)


const styles = StyleSheet.create({


    inputContainer: {
        backgroundColor: Colors.light_gray,
        marginHorizontal: width / 10,
        marginTop: width / 22,
        borderRadius: width / 10,
        paddingHorizontal: 10,
        height: width / 10,
        flexDirection: "row", alignItems: "center",
    },
    inputText: {
        color: Colors.dark,
        // fontFamily: 'PoppinsLight',
        fontSize: width / 32, flex: 1, paddingLeft: 7, marginBottom: -(width / 80)
        //   paddingTop : width / 30
    }


})
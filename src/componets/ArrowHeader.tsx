/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-18 21:19:15
 */

import React, { useEffect } from 'react';
import { Text, Dimensions, StyleSheet, TouchableOpacity, View, Image } from 'react-native'
import { Colors, GlobalStyles, Icons } from '../theme'
import AppStatusbar from './AppStatusbar';
import BackArrow from './BackArrow';

var { height, width } = Dimensions.get('window')

type ArrowHeaderType = {
    logoIcon?: boolean
}

const Header = ({ logoIcon }: ArrowHeaderType) => {

    return (
        <>
            <AppStatusbar />

            <View style={{ height: width / 8, flexDirection: "row", alignItems: "center", }}>

                <BackArrow style={GlobalStyles.backArrow} />

                <View style={{ flex: 1, }} />

                {logoIcon &&
                    <Image source={Icons.logo}
                        style={{
                            width: width / 15,
                            height: height / 15, resizeMode: "contain", marginRight: 20
                        }} />}

            </View>
        </>
    )
}


export default Header;

const styles = StyleSheet.create({

    btnContainer: {
        backgroundColor: Colors.blue, marginHorizontal: 50,
        height: 50, justifyContent: "center", alignItems: "center",
        borderRadius: 7,

    },
    HeaderText: {
        color: Colors.white, fontSize: width / 25
    }

})
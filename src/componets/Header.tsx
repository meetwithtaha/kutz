/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-03-31 20:34:06
 */

import React, { memo } from 'react';
import { Dimensions, StyleSheet, View, Image, StyleProp, ViewStyle } from 'react-native'
import { Colors, Icons } from '../theme'
import AppStatusbar from './AppStatusbar';
import { Avatar } from 'react-native-paper';
import TextView from './TextView';
import BackArrow from './BackArrow';
import { MenuButton } from '.';

var { width } = Dimensions.get('window')


type HeaderType = {
    style?: StyleProp<ViewStyle>
    label?: string
    expend?: boolean
    shouldBack?: boolean
}

const Header = ({ style, label, expend, shouldBack = false}: HeaderType) => {


    return (
        <>
            <AppStatusbar dark darkBrown />

            <View style={[styles.container, { height: expend ? (width / 2.5) : (width / 6), }, style]}>

                <View style={{ flexDirection: "row" }}>

                    <View style={{ flex: expend ? 1 : 0, }}>

                        {shouldBack ?
                            <BackArrow  tintColor={"white"} />
                            : <MenuButton />

                        }

                        {expend &&
                            <>
                                <BackArrow tintColor={"white"} style={{ marginTop: -2 }} />

                                <TextView text={"Welcome to KUTZS"} style={{ color: Colors.white, marginLeft: width / 25, }} />
                            </>}

                    </View>

                    {expend &&
                        <View style={{ alignItems: "center", alignSelf: "flex-end", marginRight: 10 }}>
                            <Avatar.Image size={width / 5}
                                source={Icons.male} style={{ backgroundColor: "transparent" }} />

                            <TextView text={"Katrina Kapoor"}
                                style={{ textAlign: "center", color: Colors.white }} />

                        </View>
                    }

                </View>


                <TextView type={"headingMid"} text={label} style={{ color: Colors.white }} />

            </View>

            <Image
                source={Icons.line}
                style={{ width, height: width / 50, resizeMode: "cover", }}
            />

        </>
    )
}


export default memo(Header);

const styles = StyleSheet.create({

    container: {
        backgroundColor: Colors.brown,
        flexDirection: "row",
        alignItems: "center",
        // marginTop: width / 15, // ios
    },

    arrowContainer: {

        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginHorizontal: width / 40

    },



})
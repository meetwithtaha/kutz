import React from 'react'
import { Animated, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'


const PressAnimation = ({ children, style, onPress }: any) => {


    const animatedValue = new Animated.Value(0)

    const animatedValueInterpolateScale = animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0.90]
    })

    const pressInHandler = () => {
        Animated.timing(
            animatedValue,
            {
                toValue: 1,
                duration: 50,
                useNativeDriver: true,
            }
        ).start()
    }

    const pressOutHandler = () => {
        Animated.timing(
            animatedValue,
            {
                toValue: 0,
                duration: 50,
                useNativeDriver: true,
            }
        ).start()
    }


    return (
        <Animated.View style={[
            {
                transform: [{ scaleX: animatedValueInterpolateScale }, { scaleY: animatedValueInterpolateScale }],
            }
        ]}>

            <TouchableOpacity
                activeOpacity={1}
                onPressIn={pressInHandler} onPressOut={pressOutHandler}
                onPress={onPress}>
                {children}
            </TouchableOpacity>
        </Animated.View>
    )
}


export default PressAnimation

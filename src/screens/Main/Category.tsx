/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-20 00:39:33
 */

import React, { } from 'react';
import { View, StyleSheet, Dimensions, Image, ScrollView, Animated, Platform } from 'react-native'
import { GlobalStyles, Icons } from '../../theme';
import { TextView, Button, ArrowHeader } from '../../componets';

var { height, width } = Dimensions.get('window')


let screenWidth = Dimensions.get('window').width
let itemSize = screenWidth * .6
let spacer_item = (screenWidth - itemSize) / 2

const catgories = [{ key: 'empty' }, ...[{ icon: Icons.female, slider: Icons.dot1 }, { icon: Icons.male, slider: Icons.dot2 }, { icon: Icons.kids, slider: Icons.dot3 },], { key: 'empty' }]


const Category = ({ navigation }: any) => {


    const onNext = () => {
        navigation.navigate('Home')
    }

    const scrollX = React.useRef(new Animated.Value(0)).current;
    const scrollXList = React.useRef(new Animated.Value(0)).current;

    var renderItem = ({ item, index }: any) => {
        if (item.key == 'empty') {
            return <View style={{ width: spacer_item }} />
        }

        const inputRange = [
            (index - 2) * itemSize,
            (index - 1) * itemSize,
            index * itemSize,
        ];

        const translateY = scrollX.interpolate({
            inputRange,
            outputRange: [-20, 30, -20],
            extrapolate: 'clamp',
        });

        let scale = scrollX.interpolate({
            inputRange,
            outputRange: [.5, 1.5, .5],
            extrapolate: 'clamp'
        })

        return (
            <View style={{ width: itemSize }}>

                <Animated.View
                    style={{ transform: [{ translateY }] }}>

                    <Animated.Image source={item.icon}
                        style={[styles.icon, {
                            transform: [{ scale: scale }],
                        }]} />

                    <Animated.Image source={item.slider}
                        style={[styles.slider, { transform: [{ scale: scale }], }]} />

                </Animated.View>
            </View>

        )
    }

    var renderItemList = ({ item, index }: any) => {
        if (item.key == 'empty') {
            return <View style={{ width: spacer_item }} />
        }

        const inputRange = [
            (index - 2) * itemSize,
            (index - 1) * itemSize,
            index * itemSize,
        ];

        const translateY = scrollXList.interpolate({
            inputRange,
            outputRange: [.4, 1, .4],
            extrapolate: 'clamp',
        });


        return (
            <View style={styles.itemContainer}>

                <Animated.View
                    style={{
                        opacity: translateY
                    }}>


                    <Image source={Icons.category_item}
                        style={styles.itemImage}
                    />

                </Animated.View>
            </View>

        )
    }


    return (
        <View style={[GlobalStyles.container,]}>
            <ArrowHeader />

            <Image source={Icons.bottom_yellow}
                style={styles.backgroundImage}
            />

            <ScrollView>

                <TextView type={'heading'} text={"Select Category"} />

                <Animated.FlatList
                    data={catgories}
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    bounces={false}
                    contentContainerStyle={{ paddingBottom: height / 13 }}
                    decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
                    snapToInterval={itemSize}
                    snapToAlignment='start'
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                        { useNativeDriver: true }
                    )}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />


                <Animated.FlatList
                    data={catgories}
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    bounces={false}
                    decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
                    snapToInterval={itemSize}
                    snapToAlignment='start'
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { x: scrollXList } } }],
                        { useNativeDriver: true }
                    )}
                    renderItem={renderItemList}
                    keyExtractor={(item, index) => index.toString()}
                />


                <Button label={'Proceed'} style={styles.button} onPress={onNext} />



            </ScrollView>




        </View>

    )

}


export default Category


const styles = StyleSheet.create({

    icon: {
        width: width / 2, height: height / 6,
        resizeMode: "contain", marginTop: height / 50, alignSelf: "center"
    },

    backgroundImage: {

        width: width , height: width / 3.9,
        resizeMode: "contain", position: "absolute",
        bottom: -5,
        left: 0, right: 0

    },

    button: { marginTop: height / 40, marginBottom: height / 10 },

    slider: {

        alignSelf: "center", height: height / 50, width: width / 8, resizeMode: "contain", marginTop: height / 22
    },
    itemContainer: { width: itemSize, height: height / 2.3, justifyContent: 'center', },
    itemImage: {
        height: height / 2.3,
        width: width / 1.6, resizeMode: "contain", alignSelf: "center",
    }

})
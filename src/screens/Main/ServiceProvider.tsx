/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-26 03:34:01
 */


import React, { } from 'react';
import { View, StyleSheet, Dimensions, Image, ScrollView, Animated, Platform, FlatList } from 'react-native'
import { GlobalStyles, Icons, Colors } from '../../theme';
import { TextView, Button, ArrowHeader, Header, ServiceCart } from '../../componets';

var { height, width } = Dimensions.get('window')

// import Icon from 'react-native-vector-icons/AntDesign';

const servicesType = [{ key: 'empty' }, ...[{ icon: Icons.female, slider: Icons.dot1 }, { icon: Icons.female, slider: Icons.dot1 }, { icon: Icons.female, slider: Icons.dot1 }, { icon: Icons.male, slider: Icons.dot2 }, { icon: Icons.kids, slider: Icons.dot3 },], { key: 'empty' }]

let screenWidth = Dimensions.get('window').width
let itemSize = screenWidth * .3
let spacer_item = (screenWidth - itemSize) / 2

const Menu = ({ navigation }: any) => {

    const scrollX = React.useRef(new Animated.Value(0)).current;

    const renderItem = ({ item, index }: any) => {
        if (item.key == 'empty') {
            return <View style={{ width: spacer_item }} />
        }

        const inputRange = [
            (index - 2) * itemSize,
            (index - 1) * itemSize,
            index * itemSize,
        ];

        let scale = scrollX.interpolate({
            inputRange,
            outputRange: [.5, 1, .5],
            extrapolate: 'clamp'
        })

        const opacity = scrollX.interpolate({
            inputRange,
            outputRange: [.4, 1, .4],
            extrapolate: 'clamp',
        });

        return (
            <View style={{ width: itemSize }}>

                <Animated.View
                    style={{ transform: [{ scale }], opacity }}>

                    <View style={styles.box}>

                        <TextView type="small" style={{ textAlign: "center", color: Colors.white }} text={"Hair\nCutting"} />

                    </View>

                </Animated.View>
            </View>

        )
    }

    const renderServiceItem = ({ item, index }: any) => {
        return <ServiceCart />
    }

    return (
        <View style={[GlobalStyles.container,]}>


            <Header label={"Services"} shouldBack />

            <Animated.FlatList
                data={servicesType}
                showsHorizontalScrollIndicator={false}
                horizontal
                bounces={false}
                contentContainerStyle={{ paddingBottom: height / 13 }}
                decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
                snapToInterval={itemSize}
                snapToAlignment='start'
                scrollEventThrottle={16}
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                    { useNativeDriver: true }
                )}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />

            <FlatList
                data={servicesType}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: height / 13 }}
                renderItem={renderServiceItem}
                keyExtractor={(item, index) => index.toString()}
            />





        </View>

    )

}


export default Menu


const styles = StyleSheet.create({

    box: {
        width: width / 5, height: width / 5, alignSelf: "center",
        borderRadius: width / 20, backgroundColor: Colors.pink,
        marginTop: 20, justifyContent: "center", alignItems: "center"
    },



})

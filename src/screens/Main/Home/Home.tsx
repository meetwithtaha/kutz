import React, { } from 'react';
import { View, Image, StyleSheet, Dimensions } from 'react-native';
import { GlobalStyles, Icons } from '../../../theme';
import { AppStatusbar, TextInput, SearchHeader } from '../../../componets';

const { height, width } = Dimensions.get('window')

const Home = () => {
    return (
        <View style={GlobalStyles.container}>
            <AppStatusbar darkBrown dark />


            <Image
                source={Icons.slider_1}
                style={{ height: height / 2.2, width, resizeMode: "cover" }}
            />

            <Image
                source={Icons.circle}
                style={{ height: height / 2, width: width - 10, resizeMode: "contain", }}
            />


            <SearchHeader style={{ position: 'absolute', top: height * .04 }} />

        </View>
    );
}

export default Home;

const styles = StyleSheet.create({




})
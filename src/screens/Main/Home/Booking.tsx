import React, { } from 'react';
import { View, StyleSheet, Dimensions, FlatList } from 'react-native';
import { GlobalStyles, Colors, Icons } from '../../../theme';
import { Header, TextView, BookingCart, BookingTab, TextInput } from '../../../componets';
import { Avatar } from 'react-native-paper';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const { height, width } = Dimensions.get('window')
const Tab = createMaterialTopTabNavigator();


const BookingData = [
    { color: '#2980b9' },
    { color: '#9b59b6' },
    { color: '#2ecc71' },
    { color: '#c0392b' },
]

const Booking = () => {

    const renderItem = ({ item, index }: any) => {
        return <BookingCart style={{ backgroundColor: item.color }} />
    }

    return (
        <View style={GlobalStyles.container}>

            <TextView text="Mon, 3 March 2021" style={{ marginTop: height / 40, marginLeft: width / 30, }} />


            <FlatList
                data={BookingData}
                contentContainerStyle={{ paddingBottom: height / 20 }}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />

        </View>

    );
}

const Tabs = () => {
    return <View style={GlobalStyles.container}>

        <Header label={"Booking"} />

        <TextInput placeholder={"Filter by Booking ID"}
            onChangeText={(value) => { }}
            value={''}
            search
            style={{ marginBottom: height / 80, marginTop: height / 80 }}
        />


        <Tab.Navigator backBehavior='initialRoute' initialRouteName='Show all'
            tabBar={(props) => {
                return <BookingTab {...props} />
            }}>

            <Tab.Screen name="Show all" component={Booking} />
            <Tab.Screen name="Upcoming" component={Booking} />
            <Tab.Screen name="Pending" component={Booking} />
            <Tab.Screen name="Completed" component={Booking} />
            <Tab.Screen name="Cancelled" component={Booking} />

        </Tab.Navigator>



    </View>


}


export default Tabs;



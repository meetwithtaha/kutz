/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-24 20:41:44
 */

import React from 'react';
import { Image, ImageSourcePropType, Dimensions } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Home, Booking, Videos, Messages } from '../..'
import { Icons, Colors } from '../../../theme';
import { TextView } from '../../../componets';

const Tab = createBottomTabNavigator();

const { height, width } = Dimensions.get('window')

const Tabs = () => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.pink,
                inactiveTintColor: Colors.white,
                showLabel: false,
                activeBackgroundColor: Colors.white,
                inactiveBackgroundColor: Colors.pink,
                style: {
                    backgroundColor: Colors.pink,
                    height: height / 12,
                },
            }}>
            <Tab.Screen name="Home" component={Home}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <TabBarIcon icon={Icons.home} color={color} label="Home" />
                    }
                }}
            />
            <Tab.Screen name="Booking" component={Booking}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <TabBarIcon icon={Icons.Booking} color={color} label="Booking" />
                    }
                }}

            />
            <Tab.Screen name="Videos" component={Videos}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <TabBarIcon icon={Icons.videos} color={color} label="Videos" />
                    }
                }}


            />
            <Tab.Screen name="Messages" component={Messages}
                options={{
                    tabBarIcon: ({ color }) => {
                        return <TabBarIcon icon={Icons.ic_chat} color={color} label="Messages" />
                    }
                }}
            />
        </Tab.Navigator>
    );
}

export default Tabs

type TabBarIconType = {
    icon: ImageSourcePropType
    label: string
    color: string
}

var TabBarIcon = ({ icon, label, color }: TabBarIconType) => {
    return (
        <>
            <Image
                source={icon}
                style={{
                    resizeMode: 'contain',
                    width: width / 3, height: width / 18, tintColor: color,
                    marginTop: height / 150
                }}
            />

            <TextView type={'normalRg'} text={label} style={{ color, marginTop: height / 150 }} />
        </>

    )
}
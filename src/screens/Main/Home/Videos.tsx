import React, { } from 'react';
import { View, FlatList, Image, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { GlobalStyles, Colors, Icons } from '../../../theme';
import { Header, PressInOut } from '../../../componets';

const VideosData = [
    { videoUri: '', thumbnail: 'https://image.freepik.com/free-photo/make-up-artist-applying-eyeshadow-woman_23-2148332525.jpg', },
    { videoUri: '', thumbnail: 'https://image.freepik.com/free-vector/cosmetic-wreath-design-with-eyelash-curler-eyeliner-brush_83728-1850.jpg', },
    { videoUri: '', thumbnail: 'https://image.freepik.com/free-vector/set-make-up-brushes-products-frame_25030-40100.jpg', },
]

const { height, width } = Dimensions.get('window')


const Videos = () => {

    const renderItem = ({ item, index }: any) => {

        return (
            <PressInOut onPress={() => {
                alert('Will play video')
            }}>

                <View style={styles.imageContainer}>

                    <Image
                        source={{ uri: item.thumbnail }}
                        style={styles.image}
                    />


                    <Image
                        source={Icons.play}
                        style={{
                            height: height / 10, width: width / 5,
                            resizeMode: "contain",
                            alignSelf: "center",
                            // position: "absolute", 
                        }}
                    />

                </View>

            </PressInOut >
        )
    }

    return (
        <View style={GlobalStyles.container}>

            <Header label={"Videos"} />

            <FlatList
                data={VideosData}
                contentContainerStyle={{ paddingBottom: height / 20 }}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />

        </View>
    );
}

export default Videos;


const styles = StyleSheet.create({

    image: {
        height: height / 3.5, width: width - 20,
        alignSelf: "center", resizeMode: "contain",
        borderWidth: 1, borderColor: "#eee", position: "absolute",
    },

    imageContainer: {
        justifyContent: 'center', height: height / 3.5, marginTop: height / 80,
    },

})
/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-20 00:39:33
 */

import React, { } from 'react';
import { View, StyleSheet, Dimensions, Image, ScrollView, Animated, Platform, TouchableOpacity } from 'react-native'
import { GlobalStyles, Icons, Colors } from '../../theme';
import { TextView, Button, ArrowHeader, Header } from '../../componets';

var { height, width } = Dimensions.get('window')

// import Icon from 'react-native-vector-icons/AntDesign';

const Menu = ({ navigation }: any) => {


    return (
        <View style={[GlobalStyles.container,]}>


            <Header expend />

            <View style={{ marginTop: height / 70 }} />

            <MenuItem label={"Home Page"} onPress={() => {
                navigation.goBack();
            }} />

            <MenuItem label={"Service Provider"}
                onPress={() => {
                    navigation.navigate('ServiceProvider');
                }} />

            <MenuItem label={"Language Settings"}
                onPress={() => {

                }} />

            <MenuItem label={"Beauty Tips"} />
            <MenuItem label={"Expert Advice"} />
            <MenuItem label={"FAQs"} />
            <MenuItem label={"Help"} />
            <MenuItem label={"Terms & Conditions"} />
            <MenuItem label={"Design You Own Package"} />


            <View style={styles.bottomView}>

                <TextView text={"Version 1.2.0"}
                    style={styles.text} />

                <Image
                    source={Icons.logout}
                    style={styles.logout}
                />

            </View>

        </View>

    )

}


export default Menu


const styles = StyleSheet.create({

    bottomView: {

        width: width,
        height: height / 10, backgroundColor: Colors.pink, justifyContent: 'space-between',
        flexDirection: "row", alignItems: "center",
        position: "absolute", bottom: 0

    },

    text: {
        textAlign: "left", color: Colors.white, marginLeft: height / 50, marginTop: height / 200
    },

    logout: {
        width: width / 10, height: height / 10, resizeMode: "contain", marginRight: width / 20
    }

})

type MenuItemType = {
    label: string
    onPress(): void
}

var MenuItem = ({ label, onPress }: MenuItemType) => {
    return (
        <TouchableOpacity activeOpacity={.9} style={{
            marginTop: height / 70,
            width: width - 40,
            height: height / 20, backgroundColor: Colors.pink, justifyContent: 'space-between',
            alignSelf: "center", borderRadius: 7, flexDirection: "row", alignItems: "center"
        }} onPress={onPress}>

            <TextView text={label}
                style={{ textAlign: "left", color: Colors.white, marginLeft: height / 50, marginTop: height / 200 }} />

            {/* <Icon name="caretright" size={width / 30} color={Colors.white} style={{ marginRight: height / 50 }} /> */}

        </TouchableOpacity>
    )
}
/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-04 20:05:02
 */


import React, { useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, Text, } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../../theme';
import { CurveView, TextView, Cursor, Slider } from '../../componets';

var { height, width } = Dimensions.get('window')


const SlideOne = ({ navigation }: any) => {

    // useEffect(() => {

    //     navigation.navigate('SlideTwo')

    // }, [])

    const onNext = () => {
        // navigation.navigate('SlideTwo')
        navigation.navigate('Category')
        
    }

    return (
        <View style={GlobalStyles.container}>

            <CurveView
                bgImage={Icons.groom_black}>

                <View>

                    <Image
                        source={Icons.dot1}
                        style={GlobalStyles.dots}
                    />

                    <TextView type={'mini_heading22'} text={"Find and Book Services"} />
                    <TextView type={'description13'} text={"Find and book Barber, Beauty, Salon &\nSpa services anywhere, anytime."} />

                    <Slider position={"left"} style={GlobalStyles.sliderComp}
                        onNext={onNext}
                    />

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: width / 20,
                        marginHorizontal: width / 5,
                    }}>

                        <TextView type={'mini_description'} text={"NEXT"}
                            style={{ flex: 1, textAlign: "right" }}
                        />


                    </View>

                </View>

            </CurveView>

        </View>
    )

}


export default SlideOne;

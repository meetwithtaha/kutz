/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-04 20:05:02
 */


import React, { useState, useRef } from 'react';
import { View, StyleSheet, Dimensions, Animated, Image, } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../../theme';
import { CurveView, TextView, Slider } from '../../componets';

var { width, height } = Dimensions.get('window')


const duration = 500;

const SlideTwo = ({ navigation }: any) => {


    var [showView, setShowView] = useState<boolean>(false)

    var sliderAni = useRef(new Animated.Value(0)).current;
    var textAni = useRef(new Animated.Value(0)).current;

    const executeTextAnimation = (action: string) => {
        Animated.parallel([
            Animated.timing(sliderAni, {
                toValue: action == "revert" ? 0 : 10,
                duration: duration,
                useNativeDriver: true,
            }),
            Animated.timing(textAni, {
                toValue: action == "revert" ? 0 : -width / 8,
                duration: duration,
                useNativeDriver: true,
            }),
        ]).start()
    }

    const onNext = () => {
        if (global.showView) {

            global.showView = false
            navigation.navigate('Login')

        } else {
            global.showView = true
            setShowView(true)
            executeTextAnimation("forward")
        }
    }

    const onPrevious = () => {
        if (global.showView) {

            executeTextAnimation("revert")
            setShowView(false)
            global.showView = false

        } else {
            navigation.goBack()
            global.showView = false
        }
    }


    return (
        <View style={GlobalStyles.container}>


            <CurveView
                bgImage={showView ? Icons.color_hair : Icons.wash_hair}>

                <View>

                    <Image
                        source={showView ? Icons.dot3 : Icons.dot2}
                        style={GlobalStyles.dots}
                    />

                    <TextView type={'mini_heading22'} text={"Styles that Suit your Lifestyle"} />
                    <TextView type={'description13'} text={"Choose our Makeup special offer price Package\nthat fit your Lifestyle."} />


                    <Animated.View
                        style={{
                            transform: [{ translateY: sliderAni }]
                        }}>

                        <Slider style={GlobalStyles.sliderComp}
                            onNext={onNext}
                            onPrevious={onPrevious}
                        />

                    </Animated.View>

                    <Animated.View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: width / 20,
                        marginHorizontal: width / 5,
                        transform: [{ translateY: textAni }]
                    }}>

                        <TextView type={'mini_description'} text={"PREVIOUS"} style={{ flex: 1, textAlign: "left" }} />

                        <TextView type={'mini_description'} text={showView ? "GET STARTED" : "NEXT"} />

                    </Animated.View>


                    {showView &&
                        <View style={styles.welcomeNote}>

                            <View style={{ flex: 1 }}>
                                <TextView type={'normal'} text={"Welcome Screen"} />
                                <TextView type={'mini_description'} text={"Always Show"} />
                            </View>


                            <SwitchComp onPress={() => {
                                // navigation.navigate('Login')
                            }} />
                        </View>
                    }
                </View>

            </CurveView>


        </View>
    )

}


export default SlideTwo;


import { Switch } from 'react-native-paper';

var SwitchComp = ({ onPress }: any) => {
    const [isSwitchOn, setIsSwitchOn] = React.useState(true);



    return (
        <Switch value={isSwitchOn} color={'white'} onValueChange={(value) => {
            setIsSwitchOn(value)
        }}
            trackColor={{ true: Colors.pink, false: '#eee' }}
        />
    )
}

const styles = StyleSheet.create({


    welcomeNote: { flexDirection: "row", marginHorizontal: 30, position: "absolute", bottom: -(height / 10) }


})
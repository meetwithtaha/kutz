/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-05 23:33:13
 */


import React, { useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, Text, } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../../theme';
import { CurveView, TextView, Cursor, Slider, Button, TextInput } from '../../componets';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';


var { height, width } = Dimensions.get('window')


const Otp = ({ navigation }: any) => {

    var [code, setCode] = useState("")
    let [timer, setTimer] = useState(10)
    let [showResend, setShowResend] = useState(false)



    useEffect(() => {
        let interval = null
        if (!showResend) {
            interval = setInterval(
                () => {
                    let nTimer = --timer
                    setTimer(nTimer)
                    if (nTimer < 1) {
                        setShowResend(true)
                        setTimer(30)
                        clearInterval(interval);
                    }


                },
                1000
            );
        }
        return () => {
            if (interval != null) {
                clearInterval(interval);
            }

        }
    }, [showResend])

    const resendCode = () => {
        setShowResend(false)
    }

    const verifyCode = () => {
        navigation.navigate('Gender')
    }


    return (
        <View style={GlobalStyles.container}>

            <CurveView
                bgImage={Icons.groom}>

                <View style={{ alignItems: "center" }}>

                    <TextView type={'heading'} text={"Phone Verification"} />
                    <TextView type={'description13'} text={"Enter your OTP code here"} />


                    <SmoothPinCodeInput autoFocus={true}

                        cellStyle={{
                            borderColor: Colors.transparent,
                            borderWidth: 1,
                            marginLeft: 7, marginRight: 7,
                            marginVertical: height / 2,
                            alignSelf: "center",
                            backgroundColor: Colors.light_gray, borderRadius: width / 10,
                        }}
                        textStyle={{
                            fontSize: 20,
                            color: Colors.black
                        }}
                        textStyleFocused={{
                            color: 'white'
                        }}
                        containerStyle={{
                            marginVertical: height / 25,

                        }}

                        cellStyleFocused={{
                            borderColor: Colors.pink,
                            marginLeft: 7, marginRight: 7, borderRadius: width / 15,
                            backgroundColor: Colors.pink, elevation: 12
                        }}
                        value={code}
                        onTextChange={(code: any) => setCode(code)}
                        onFulfill={() => { }}
                        codeLength={4}

                        onBackspace={() => {
                            // console.log('No more back.')
                        }}
                    />

                    <TextView type={'normalRg'}
                        text={"00:" + (timer > 9 ? timer : "0" + timer)}
                        style={{ color: Colors.ddark, marginBottom: height / 25, }} />


                    <View style={styles.social}>

                        <TextView type={'normalRg'} text={"Didn’t you receive any code?"} style={{ color: Colors.ddark }} />
                        <TextView type={'normalMd'} text={"Resend a new code"} style={{ color: Colors.pink_dark, marginLeft: width / 80 }} />

                    </View>


                    <Button label={'Verify'} onPress={verifyCode} />

                </View>


            </CurveView>

        </View>
    )

}


export default Otp


const styles = StyleSheet.create({


    social: { alignItems: 'center', justifyContent: 'center', marginBottom: height / 50 }


})
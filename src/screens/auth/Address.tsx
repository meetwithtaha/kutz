import React, { useState } from 'react';
import { View, StyleSheet, Dimensions, Image, } from 'react-native'
import { GlobalStyles, Icons, Colors } from '../../theme';
import { TextView, Button, BackArrow, CurveView, TextInput } from '../../componets';

var { height, width } = Dimensions.get('window')

const Address = ({ navigation }: any) => {

    const onNext = () => {
        navigation.navigate('DOB')
    }

    return (
        <View style={[GlobalStyles.container]}>

            <CurveView
                bgImage={Icons.groom}>

                <View style={{ flex: 1, marginTop: 16 }}>

                    <TextView type={'heading'} text={"Add a New Address"} />


                    <View style={styles.grid}>

                        <Button style={{ width: width / 4 }}
                            label={'Home'} onPress={() => { }} />

                        <Button style={{ width: width / 4 }}
                            label={'Work'} onPress={() => { }} />

                        <Button style={{ width: width / 4 }}
                            label={'Other'} onPress={() => { }} />

                    </View>


                    <TextInput
                        placeholder={"Address Title"}
                        onChangeText={(value) => {
                            alert(value)
                        }}
                    />


                    <TextInput
                        placeholder={"Floor / Suit / Unit #"}
                        onChangeText={(value) => {
                            alert(value)
                        }}
                    />


                    <TextInput
                        placeholder={"Select City"}
                        onChangeText={(value) => {
                            alert(value)
                        }}
                    />


                    <TextInput
                        placeholder={"Select Area"}
                        onChangeText={(value) => {
                            alert(value)
                        }}
                    />


                    <TextInput
                        numberOfLines={3}
                        placeholder={"Notes"}
                        onChangeText={(value) => {
                            alert(value)
                        }}
                    />


                </View>

            </CurveView>



        </View>
    )

}


export default Address


const styles = StyleSheet.create({

    headerContainer: {
        flex: .1, flexDirection: "row", marginHorizontal: 30,
    },
    grid: {
        flex: 1, flexDirection: "row", justifyContent: "space-between", marginHorizontal: width / 15
    },

})
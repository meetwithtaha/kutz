/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-05 23:33:13
 */


import React, { useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, Text, } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../../theme';
import { CurveView, TextView, Cursor, Slider, Button, TextInput } from '../../componets';

var { height, width } = Dimensions.get('window')


const Login = ({ navigation }: any) => {

    var [phoneNumber,  setPhoneNumber] = useState<string>()

    var onLoginPress = () => {
        navigation.navigate('Otp')
    }

    const onClear = () => {
        setPhoneNumber("")
    }

    return (
        <View style={GlobalStyles.container}>

            <CurveView
                bgImage={Icons.groom}>

                <View>

                    <TextView type={'heading'} text={"Verification"} />
                    <TextView type={'description13'} text={"We have sent you an SMS with a code\nto number +91\n8586023109"} />

                    <TextInput placeholder={"Enter your number"}
                        onChangeText={(value) => setPhoneNumber(value)}
                        value={phoneNumber}
                        style={{ marginBottom: height / 12 }}
                        icon
                        country
                        keyboardType={'phone-pad'}
                        onClear={onClear}
                    />

                    {/* <View style={styles.social}>

                        <TextView type={'normalRg'} text={"Or Login with"} style={{ color: Colors.ddark }} />
                        <TextView type={'normalMd'} text={"Social Network"} style={{ color: Colors.pink_dark, marginLeft: width / 80 }} />

                    </View> */}

                    <Button label={'Continue'} onPress={onLoginPress} />


                </View>

 

            </CurveView>

        </View>
    )

}


export default Login


const styles = StyleSheet.create({


    social: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: height / 40, marginBottom: height / 12 }


})
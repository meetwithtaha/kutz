/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-16 16:47:35
 */



import React, { } from 'react';
import { View, StyleSheet, Dimensions, Image, Text, ScrollView } from 'react-native'
import { GlobalStyles, Icons, Colors } from '../../theme';
import { TextView, Button, TextInput, BackArrow, ArrowHeader } from '../../componets';
import CalendarPicker from 'react-native-calendar-picker';



var { height, width } = Dimensions.get('window')



const UploadImage = ({ navigation }: any) => {

    const onNext = () => {
        navigation.navigate('TermsConditions')
    }

    return (
        <View style={[GlobalStyles.container]}>

            <ArrowHeader />

            <ScrollView contentContainerStyle={[GlobalStyles.container, , { flex: 0, flexGrow: 1, justifyContent: 'center', }]}>

                <View style={{ height: height * .7, justifyContent: "center", }}>

                    <TextView type={'heading'} text={"Upload Profile Photo"} />

                    <View style={{ flexDirection: "row", alignItems: "flex-end", justifyContent: 'center', marginTop: (height / 50) }}>


                        <Image
                            source={Icons.camera}
                            style={{
                                width: width / 1.5, height: width / 1.7,
                                resizeMode: "contain", alignSelf: "center"
                            }}
                        />

                        <Image
                            source={Icons.upload}
                            style={{
                                width: width / 7, height: height / 7,
                                resizeMode: "contain",
                                marginLeft: -width / 6
                                // position: "absolute", bottom: -15, right: 20, 
                            }}
                        />

                    </View>

                </View>

                <Button label={'Next'} style={{ marginTop: height / 20, }}
                    onPress={onNext} />

            </ScrollView>
        </View>
    )

}


export default UploadImage



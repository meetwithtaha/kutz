/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-16 16:47:35
 */



import React, { } from 'react';
import { View, StyleSheet, Dimensions, Image, Text, SafeAreaView, ScrollView } from 'react-native'
import { GlobalStyles, Icons, Colors } from '../../theme';
import { TextView, Button, TextInput, BackArrow, AppStatusbar, ArrowHeader } from '../../componets';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import CalendarPicker from 'react-native-calendar-picker';

var { height, width } = Dimensions.get('window')

const DOB = ({ navigation }: any) => {


    const onNext = () => {
        navigation.navigate('UploadImage')
    }

    return (
        <View style={[GlobalStyles.container]}>

            <ArrowHeader />

            <ScrollView contentContainerStyle={[GlobalStyles.container, , { flex: 0, flexGrow: 1, justifyContent: 'center', }]}>

                <TextView type={'heading'} text={"Enter Your Details"} />

                <TextInput placeholder={"User Name"}
                    onChangeText={(value) => { }}
                    value={''}
                    keyboardType={'phone-pad'}
                />

                <TextInput placeholder={"Email Address"}
                    onChangeText={(value) => { }}
                    value={''}
                    keyboardType={'phone-pad'}
                />

                <TextInput placeholder={"DD/MM/YYYY"}
                    onChangeText={(value) => { }}
                    value={''}
                    keyboardType={'phone-pad'}
                />

                <View style={{
                    backgroundColor: "#F0F0F0", width: width - 70,
                    alignSelf: "center", borderLeftColor: "transparent",
                    borderTopColor: "transparent", borderRadius: 18
                }}>

                    <CalendarPicker
                        scaleFactor={(height / 2 + 50) > 400 ? (height / 2 + 50) : (height / 2 + 150)}
                        dayLabelsWrapper={{ width: width - 80, borderTopWidth: 0, borderBottomWidth: 0, }}
                        monthTitleStyle={{ 
                            // fontFamily: "PoppinsLight", 
                            color: "white", fontSize: 13 }}
                        yearTitleStyle={{ 
                            // fontFamily: "PoppinsLight", 
                            color: "white", fontSize: 15 }}
                        headerWrapperStyle={{
                            backgroundColor: Colors.pink, width: width - 70,
                            borderTopRightRadius: 18, borderTopLeftRadius: 18
                        }}
                        selectedDayColor={Colors.primary}
                        // selectedTextColor={Colors.white}
                        onDateChange={(date: any) => {

                        }}
                    />

                </View>

                <Button label={'Next'} style={{ marginTop: height / 20 }} onPress={onNext} />


            </ScrollView>


        </View>

    )

}


export default DOB



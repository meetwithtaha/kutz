/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-17 15:57:26
 */



import React, { useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, Text, } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../../theme';
import { CurveView, TextView, Cursor, Slider, Button, AppStatusbar } from '../../componets';

var { height, width } = Dimensions.get('window')


const Finish = ({ navigation }: any) => {

    const onNext = () => {
        // navigation.navigate('SlideTwo')
    }

    return (
        <View style={GlobalStyles.container}>
            <AppStatusbar translucent />

            <CurveView
                bgImage={Icons.groom_black}>

                <View>

                    <Image
                        source={Icons.finish}
                        style={styles.finishIcon}
                    />

                    <TextView style={{ marginTop: height / 20, textAlign: "center" }} text={"Your Registration is Complete"} />

                    <Button label={'Proceed'} style={{ marginTop: height / 20 }} onPress={onNext} />

                </View>

            </CurveView>

        </View>
    )

}


export default Finish;


const styles = StyleSheet.create({

    finishIcon: {
        width: width / 1.5, height: height / 3.5, resizeMode: "contain", alignSelf: "center"
    },

})
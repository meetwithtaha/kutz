/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-16 16:47:35
 */



import React, { } from 'react';
import { View, StyleSheet, Dimensions, Image, ScrollView } from 'react-native'
import { GlobalStyles, Icons } from '../../theme';
import { TextView, Button, BackArrow, AppStatusbar, ArrowHeader } from '../../componets';


var { height, width } = Dimensions.get('window')


const Gender = ({ navigation }: any) => {


    const onNext = () => {
        navigation.navigate('DOB')
    }


    return (
        <View style={[GlobalStyles.container]}>

            <ArrowHeader />

            <ScrollView contentContainerStyle={[GlobalStyles.container, , { flex: 0, flexGrow: 1, justifyContent: 'center', }]}>


                <TextView type={'heading'} text={"Select Gender"} />

                <Image source={Icons.male}
                    style={styles.icon} />

                <TextView type={'heading'} text={"Male"} />

                <Image source={Icons.female}
                    style={[styles.icon, { marginTop: height / 50 }]} />

                <TextView type={'heading'} text={"Female"} />

                <Button label={'Next'} style={{ marginTop: height / 20 }} onPress={onNext} />

            </ScrollView>
        </View>
    )

}


export default Gender


const styles = StyleSheet.create({

    icon: {
        width: width / 2, height: height / 4,
        resizeMode: "contain", marginTop: height / 50, alignSelf: "center"
    },


})
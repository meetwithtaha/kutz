/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-17 00:26:49
 */

import React, { useState } from 'react';
import { View, StyleSheet, Dimensions, Image, TouchableOpacity, ScrollView, } from 'react-native'
import { GlobalStyles, Icons, Colors } from '../../theme';
import { TextView, Button, ArrowHeader } from '../../componets';

var { height, width } = Dimensions.get('window')
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Gender = ({ navigation }: any) => {

    const [beautyTips, setBeautyTips] = useState(true);
    const [advice, setAdvice] = useState(true);


    const onNext = () => {
        // navigation.navigate('Address')
        navigation.navigate('Finish')
    }


    return (
        <View style={[GlobalStyles.container]}>

            <ArrowHeader logoIcon />

            <ScrollView contentContainerStyle={[GlobalStyles.container, ,
            { flex: 0, flexGrow: 1, alignItems: "center", }]}>


                <View style={styles.fivthPercent}>

                    <TextView type={'heading'} text={"I want to Receive"} />

                    <TouchableOpacity style={styles.checkBox}
                        activeOpacity={.8}
                        onPress={() => {
                            setBeautyTips(!beautyTips)
                        }}>

                        {/* <Icon name={beautyTips ? "check-circle" : "checkbox-blank-circle"} size={24} color={beautyTips ? Colors.pink : Colors.light_gray} /> */}

                        <TextView type={'normalRg'} text={"Get Beauty Tips"} style={styles.checkBoxText} />

                    </TouchableOpacity>

                    <TouchableOpacity style={styles.checkBox}
                        activeOpacity={.8}
                        onPress={() => {
                            setAdvice(!advice)
                        }}>

                        {/* <Icon name={advice ? "check-circle" : "checkbox-blank-circle"} size={24} color={advice ? Colors.pink : Colors.light_gray} /> */}

                        <TextView type={'normalRg'} text={"Get Experts Advice"} style={styles.checkBoxText} />

                    </TouchableOpacity>


                </View>

                <View style={styles.thirdPercent}>


                    <Image source={Icons.comb_male} style={{ width: width / 2, height: height / 4, resizeMode: "contain", }} />


                    <View style={styles.social}>

                        <TextView type={'normalRg'} text={"I accept all"} style={{ color: Colors.ddark }} />
                        <TextView type={'normalMd'} text={"Terms & Conditions"} style={{ color: Colors.pink_dark, marginLeft: width / 80 }} />

                    </View>

                    <Button label={'Next'} style={{ marginTop: 10 }} onPress={onNext} />

                </View>

            </ScrollView>

        </View>
    )

}


export default Gender


const styles = StyleSheet.create({

    headerContainer: {
        flex: .1, alignItems: "center",
        justifyContent: "space-between", flexDirection: "row", marginHorizontal: 30, marginTop: height / 30
    },

    social: {
        flexDirection: 'row', alignItems: 'center',
        marginTop: height / 40,
    },
    checkBox: {
        flexDirection: "row", alignItems: "center",
        marginLeft: width / 25, marginTop: height / 80,
    },
    checkBoxText: { marginLeft: 10, color: Colors.ddark },
    thirdPercent: { flex: .2, alignItems: "center", },
    fivthPercent: { justifyContent: "center", flex: .8 }


})
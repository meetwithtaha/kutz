/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-04-03 01:09:43
 */


export { default as Splash } from './Splash'

export { default as SlideOne } from './Onboarding/SlideOne'
export { default as SlideTwo } from './Onboarding/SlideTwo'

export { default as Login } from './auth/Login'
export { default as Otp } from './auth/Otp'
export { default as Gender } from './auth/Gender'
export { default as DOB } from './auth/DOB'
export { default as UploadImage } from './auth/UploadImage'
export { default as TermsConditions } from './auth/TermsConditions'
export { default as Finish } from './auth/Finish'

export { default as Category } from './Main/Category'

export { default as Tabs } from './Main/Home/Tabs'
export { default as Home } from './Main/Home/Home'
export { default as Videos } from './Main/Home/Videos'
export { default as Booking } from './Main/Home/Booking'
export { default as Messages } from './Main/Home/Messages'

export { default as Menu } from './Main/Menu'
export { default as ServiceProvider } from './Main/ServiceProvider'

/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-03-30 18:56:14
 */

import React, { useEffect } from 'react';
import { View, StyleSheet, Image, Dimensions, Text } from 'react-native'
import { Icons, Colors, GlobalStyles } from '../theme';
import { SlideUp, CurveView } from '../componets';

var { height, width } = Dimensions.get('window')


const Splash = ({ navigation }: any) => {

    useEffect(() => {

        setTimeout(() => {
            navigation.replace('SlideOne')
            // navigation.replace('TermsConditions')
            // navigation.navigate('DOB')
        }, 2000)

        // fetch('https://jsonplaceholder.typicode.com/todos/1')
        //     .then(response => response.json())
        //     .then(json => console.log(json))

    }, [])

    return (
        <View style={GlobalStyles.container}>

            <CurveView
                bgImage={Icons.girl}>

                <Image
                    source={Icons.logo}
                    style={styles.logo}
                />

            </CurveView>

            <Text style={{
                textAlign: "center", marginBottom: 20,
                color: Colors.light_dark_gray, 
                // fontFamily: "PoppinsRegular",
            }}>@ Kutzs 2020-21. All rights reserved.</Text>

        </View>
    )

}


export default Splash;

const styles = StyleSheet.create({

    logo: {
        alignSelf: "center", width: 100, height: 100, resizeMode: "contain"
    }

})


export default {

    primary: "#F69734",
    white: "#ffffff",
    dark: "#0A0707",
    black: '#000000',
    blue: "#0090E6",
    orange: "#F69734",
    light_gray: "#eee",
    light_dark_gray: "#999999",
    chatBubbleLeft: "#EDEEF0",
    transparent: "transparent",
    brown: "#3C3C3C",
    pink: "#FE457C",
    ddark: "#666666",
    pink_dark: "#E04957",
    green: "#96D117",
}
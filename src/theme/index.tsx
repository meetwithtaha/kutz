/**
 * @author [Taha Shaikh]
 * @email [itsshaikhtaha@gmail.com]
 * @create date 2021-03-30 19:04:00
 */


export { default as Icons } from './Icons'
export { default as Colors } from './Colors'
export { default as GlobalStyles } from './GlobalStyles'

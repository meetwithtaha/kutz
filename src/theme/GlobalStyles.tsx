import React, { useEffect } from 'react';
import { StyleSheet, Dimensions } from 'react-native'
import Colors from './Colors';
import CurveView from '../componets/CurveView';

var { height, width } = Dimensions.get('window')

var GlobalStyles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    backArrow: {
        marginLeft: width / 20, 
        // position: 'absolute', left: 20, top: 20
    },

    dots: { width: width / 5, height: height / 10, resizeMode: "contain", alignSelf: "center" },

    sliderComp: { marginTop: height / 20, marginHorizontal: width / 7 },

    buttonPosition: { position: 'absolute', bottom: 50, },

    sliderView: { width: width - 150, height: height / 25, resizeMode: "contain", alignSelf: "center", marginBottom: width / -32 },


    headerTiler: {
        flex: 1, color: Colors.black, fontSize: width / 10,
    },

    profile: {
        height: 45, width: 45, resizeMode: "contain", marginRight: 10
    },

    line: { height: 2, backgroundColor: Colors.light_gray, marginTop: 20 },


    curveView: {
        borderTopRightRadius: 40, borderTopLeftRadius: 40, backgroundColor: Colors.white, marginTop: -30, flex: 1, justifyContent: "center"
    }

})

export default GlobalStyles
var girl = require('../../assets/images/girl.png')
var logo = require('../../assets/images/logo.png')
var groom = require('../../assets/images/groom.png')
var wash_hair = require('../../assets/images/wash_hair_girl.png')
var color_hair = require('../../assets/images/color_hair_girl.png')
var groom_black = require('../../assets/images/groom_black.png')
var bar = require('../../assets/images/bar.png')
var bar_handle = require('../../assets/images/bar_handle.png')
var dot1 = require('../../assets/images/dot1.png')
var dot2 = require('../../assets/images/dot2.png')
var dot3 = require('../../assets/images/dot3.png')
var Close = require('../../assets/images/Close.png')
var arrow_down = require('../../assets/images/arrow_down.png')
var male = require('../../assets/images/male.png')
var female = require('../../assets/images/female.png')
var upload = require('../../assets/images/upload.png')
var camera = require('../../assets/images/camera.png')
var comb_male = require('../../assets/images/comb_male.png')
var finish = require('../../assets/images/finish.png')
var kids = require('../../assets/images/kids.png')
var category_item = require('../../assets/images/category_item.png')
var bottom_yellow = require('../../assets/images/bottom_yellow.png')
var slider_1 = require('../../assets/images/slider_1.png')
var circle = require('../../assets/images/circle_.png')
var home = require('../../assets/images/home.png')
var videos = require('../../assets/images/videos.png')
var Booking = require('../../assets/images/Booking.png')
var ic_chat = require('../../assets/images/ic_chat.png')
var menu = require('../../assets/images/menu.png')
var play = require('../../assets/images/play.png')
var line = require('../../assets/images/line.png')
var logout = require('../../assets/images/logout.png')
var clock = require('../../assets/images/clock.png')
var ic_note = require('../../assets/images/ic_note.png')
var ic_info = require('../../assets/images/info_mini.png')
var rating_circle = require('../../assets/images/rating_circle.png')
var book_ = require('../../assets/images/book_.png')


let Icons = {
    girl, logo, groom, wash_hair, color_hair, groom_black, bar, bar_handle, dot1, dot2, dot3,
    Close, arrow_down, male, female, camera, upload, comb_male, finish, kids, category_item,
    bottom_yellow, slider_1, circle, home, videos, Booking, ic_chat, menu, play, line, logout,
    clock, ic_note, ic_info, rating_circle, book_
};

export default Icons;
